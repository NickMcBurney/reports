export const writeFirestore = async ($fire, fireDocName, today, data) => {
  const todayRef = $fire.firestore.collection(fireDocName).doc(today)
  try {
    await todayRef.set(data)
  } catch (e) {
    console.error(e)
    return
  }
}

export const readFirestoreToday = async ($fire, fireDocName, today) => {
  const todayRef = $fire.firestore.collection(fireDocName).doc(today)

  let data = false
  try {
    data = todayRef.get().then(function(doc) {
      if (doc.exists) {
        return doc.data()
      }
    })
  } catch (e) {
    console.error(e)
  }

  return data
}

export const readFirestoreAll = async ($fire, fireDocName) => {
  const pageRef = $fire.firestore.collection(fireDocName)
  let data = []

  try {
    await pageRef.get().then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
        data.push({ 
          date: doc.id,
          data: doc.data()
        })
      })
    })
  } catch (e) {
    console.error(e)
  }

  return data
}

export const fireStoreDocName = (pageURL) => {
  return pageURL === 'https://www.oceanfinance.co.uk/' ? 'oceanfinance.co.uk' : pageURL.replace('https://www.', '').replace(/\//g, '-').replace(/-$|$/, '')
}