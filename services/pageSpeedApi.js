export default async function (key, pageUrl) {
  const { loadingExperience, lighthouseResult } = await fetch(`https://www.googleapis.com/pagespeedonline/v5/runPagespeed?key=${key}&url=${pageUrl}&strategy=mobile`).then(res => res.json())

  return {
    metrics: loadingExperience.metrics,
    originFallback: loadingExperience.origin_fallback || false,
    scoreCategory: loadingExperience.overall_category,
    lightHouseScore: lighthouseResult.categories.performance.score * 100
  }
}