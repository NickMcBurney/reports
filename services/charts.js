export const createHistoricChartData = function (historic, includeFullReport) {
  const dates = historic.map((item) => item.date)
  
  const lightHouseScore = {
    type: 'line',
    label: 'Lighthouse score',
    yAxisID: 'lighthouseScore',
    fill: false,
    backgroundColor: '#2d3748',
    borderColor: '#2d3748',
    borderWidth: 4,
    data: historic.map((item) => item.data.lightHouseScore)
  }

  const firstInputDelay = {
    label: 'First Input Delay',
    yAxisID: 'timeScores300ms',
    order: 0,
    fill: false,
    backgroundColor: '#90cdf4',
    data: historic.map((item) => (item.data.metrics['FIRST_INPUT_DELAY_MS'].percentile / 1000).toFixed(2))
  }

  const firstContentfulPaint = {
    label: 'First Contentful Paint',
    yAxisID: 'timeScores',
    order: 1,
    fill: false,
    backgroundColor: '#9ae6b4',
    data: historic.map((item) => (item.data.metrics['FIRST_CONTENTFUL_PAINT_MS'].percentile / 1000).toFixed(2))
  }

  const largestContentfulPaint = {
    label: 'Largest Contentful Paint',
    yAxisID: 'timeScores',
    order: 2,
    fill: false,
    backgroundColor: '#faf089',
    data: historic.map((item) => (item.data.metrics['LARGEST_CONTENTFUL_PAINT_MS'].percentile / 1000).toFixed(2))
  }
  
  const cumalativeLayoutShift = {
    label: 'Cumulative Layout Shift Score',
    yAxisID: 'metricScore',
    order: 3,
    fill: false,
    backgroundColor: '#fed7d7',
    data: historic.map((item) => item.data.metrics['CUMULATIVE_LAYOUT_SHIFT_SCORE'].percentile / 100)
  }

  const fieldDataScores = [
    cumalativeLayoutShift, 
    firstInputDelay, 
    largestContentfulPaint, 
    firstContentfulPaint
  ]

  const datasets = [
    lightHouseScore
  ]

  if (includeFullReport) {
    datasets.push(...fieldDataScores)
  }

  const historicScores = {
    labels: dates,
    datasets: datasets
  }

  return historicScores 
}